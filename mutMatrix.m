%%%% function to categorize the mutation combinations%%%
%%%  C ->T, C->A, C->G, A->T, A->G , A->C , A->T %%%%%%
 function [metamat mut] = mutMatrix(mutString) 

idCtoT = regexpcell(mutString,'C->T');

idCtoA = regexpcell(mutString,'C->A');

idCtoG = regexpcell(mutString,'C->G');

idAtoT = regexpcell(mutString,'A->T') ;

idAtoG = regexpcell(mutString,'A->G');

idAtoC = regexpcell(mutString,'A->C');

 mutCtoT = mutString(idCtoT);
 
 mutCtoA = mutString(idCtoA);

 mutCtoG = mutString(idCtoG);

 mutAtoT = mutString(idAtoT);

mutAtoG = mutString(idAtoG);

mutAtoC = mutString(idAtoC);

%[b m n] = unique(mutAtoG);
bp_vec = {'T', 'C','A','G'};

mutCtoA_mat = zeros(4,4); 
for i = 1:4 
    bpfirst = bp_vec(i);
    bpfirst_reg = strcat(bpfirst, '(');
    mutCtoA_bpfirst = mutCtoA(regexpcell(mutCtoA, bpfirst_reg));
    for j = 1:4
        bpsecond = bp_vec(j);
        var = strcat(')', bpsecond);
        st = mutCtoA_bpfirst(regexpcell(mutCtoA_bpfirst,var));
        len =  length(st);
        if len==0
           len=0.001
        end
     
        mutCtoA_mat(i,j) = len;
         disp([bpfirst,' C->A  ', bpsecond, len]);
     
    end
end


disp(['For C -> T mutations']);

mutCtoT_mat = zeros(4,4); 
for i = 1:4 
    bpfirst = bp_vec(i);
    bpfirst_reg = strcat(bpfirst, '(');
    mutCtoT_bpfirst = mutCtoT(regexpcell(mutCtoT, bpfirst_reg));
    for j = 1:4
        bpsecond = bp_vec(j);
        var = strcat(')', bpsecond);
        st = mutCtoT_bpfirst(regexpcell(mutCtoT_bpfirst,var));
        len = length(st);
        if len==0
           len=0.001
        end
        disp([bpfirst,' C->T  ', bpsecond, len]);
        mutCtoT_mat(i,j) = len;
         
    end
end



mutCtoG_mat = zeros(4,4); 
for i = 1:4 
    bpfirst = bp_vec(i);
    bpfirst_reg = strcat(bpfirst, '(');
    mutCtoG_bpfirst = mutCtoG(regexpcell(mutCtoG, bpfirst_reg));
    for j = 1:4
        bpsecond = bp_vec(j);
        var = strcat(')', bpsecond);
        len =  length(mutCtoG_bpfirst(regexpcell(mutCtoG_bpfirst,var)));
        if len==0
           len=0.001
        end
        disp([bpfirst,' C->G  ', bpsecond, len]);
        mutCtoG_mat(i,j) = len;
     

    end
end



mutAtoG_mat = zeros(4,4); 
for i = 1:4 
    bpfirst = bp_vec(i);
    bpfirst_reg = strcat(bpfirst, '(');
    mutAtoG_bpfirst = mutAtoG(regexpcell(mutAtoG, bpfirst_reg));
    for j = 1:4
        bpsecond = bp_vec(j);
        var = strcat(')', bpsecond);
        len = length(mutAtoG_bpfirst(regexpcell(mutAtoG_bpfirst,var)));
        if len==0
           len=0.001
        end
        disp([bpfirst,' A->G  ', bpsecond, len]);
        mutAtoG_mat(i,j) = len;
      
    end
end




mutAtoT_mat = zeros(4,4); 
for i = 1:4 
    bpfirst = bp_vec(i);
    bpfirst_reg = strcat(bpfirst, '(');
    mutAtoT_bpfirst = mutAtoT(regexpcell(mutAtoT, bpfirst_reg));
    for j = 1:4
        bpsecond = bp_vec(j);
        var = strcat(')', bpsecond);
        len = length(mutAtoT_bpfirst(regexpcell(mutAtoT_bpfirst,var)));
        if len==0
           len=0.001
        end
        mutAtoT_mat(i,j) = len;
          disp([bpfirst,' A->T  ', bpsecond, len]);
         
    end
end


mutAtoC_mat = zeros(4,4); 
for i = 1:4 
    bpfirst = bp_vec(i);
    bpfirst_reg = strcat(bpfirst, '(');
    mutAtoC_bpfirst = mutAtoC(regexpcell(mutAtoC, bpfirst_reg));
    for j = 1:4
        bpsecond = bp_vec(j);
        var = strcat(')', bpsecond);
        len = length(mutAtoC_bpfirst(regexpcell(mutAtoC_bpfirst,var)));
        if len==0
           len=0.001
        end
        mutAtoC_mat(i,j) = len;
          disp([bpfirst,' A->C  ', bpsecond, len]);
          

    end
end



metamat1 = [mutCtoG_mat; mutAtoT_mat]; 

metamat2 = [mutCtoA_mat; mutAtoC_mat];

metamat3 = [mutCtoT_mat; mutAtoG_mat];

metamat = [metamat1,metamat2,metamat3];

%save('metamat.mat','metamat');

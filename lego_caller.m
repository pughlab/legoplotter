

function [] = lego_caller(inputmymaf, output)
       mymaf_ = inputmymaf; % MAF to be read 
       outputdir = output;
       
           
    [dir , filename,  ext] = fileparts(mymaf_);
    fprintf(['input file is', '\n','\n', filename, '\n'])
    
    fieldContext = 'ref_context';
    fieldAlt = 'Tumor_Seq_Allele2';
    fieldRef = 'Reference_Allele';
    variant_type = 'SNP';
    conAltRefCol = {fieldContext; fieldRef; fieldAlt};
    skipline = 3;
    AF_threshold1 = 1;
    AF_threshold2 = 0.0;
    
 %  fprintf(['this is a test']);
    maf = readtable(mymaf_,'HeaderLines',skipline,'Delimiter','\t', 'ReadVariableNames',true,'FileType', 'text');


        % get the low allele fraction of the maf files
 %       maf_1 = maf(maf.tumor_f <= AF_threshold1, :); % MAf for low allele fraction
 %       maf_2 = maf_1(maf_1.tumor_f >= AF_threshold2, :);

        % free memory by removing the unwanted tables
        
        % if interested in SNPs:
        if strcmp(variant_type,'SNP')
                % get rows only with SNP in the variant_type column
                snprows = find(~cellfun('isempty',strfind(maf.Variant_Type, 'SNP')));
                % get the part of maf with SNP as variant_type
                mafnew = maf(snprows,:);

        end


        % if interested in DNPs:
%        if strcmp(variant_type, 'DNP')
            % get rows only with DNPs in the variant_type column
%            dnprows =  find(~cellfun('isempty',strfind(maf.Variant_Type, 'DNP')));
%            mafnew = maf(dnprows,:);
%        end

     % get the three necessary columns : Context , Alt and Ref 
     contexttable  =  mafnew(:, conAltRefCol);   

    % get the mutation context matrix
     mutString = mutstring(contexttable);
    % % make the mutation count matrix from the contexttable
     metamat = mutMatrix(mutString);
     % plot the mutation count matrix in a leg-plots scheme 
     % make the filename and path to file
     fprintf(['filename is', '\n', filename])
     strnew = regexprep(filename,'.call_stats_keep.maf.txt','_LegoPlots');
     strnew = regexprep(strnew, '_',''); 
     fprintf(['str new is ', '\n', strnew])
     plot_metamat(metamat);
     
     plotfilename = strcat(strnew, '.pdf');
     
     fprintf([plotfilename])
     set(gcf,'Renderer','zbuffer') 
     %set(gcf,'position',get(0,'screensize'))
     saveas(gcf,fullfile(outputdir,plotfilename),'pdf'); 


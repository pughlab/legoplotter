%%%%%
% function to get build the context%%%
    function [mutString] = context(table)
        
         % original values for ref alt left and right flanks   
        contextstring = char(table.ref_context);
         len = length(strtrim(contextstring));
       
         mid = (len+1)/2;
      if (len==21)
     %  if (strfind('ACGT', leftFlank) && strfind('ACGT',  rightFlank) && strfind('ACGT', altstring) && strfind('ACGT',refstring) && len==21)
         leftFlank = upper(contextstring(mid - 1));
         rightFlank = upper(contextstring(mid + 1));
         altstring = upper(char(table.Tumor_Seq_Allele2));
         refstring = upper(char(table.Reference_Allele));
         fprintf(['ref string is ', refstring, '\n']);
         fprintf (['alt string is ', altstring, '\n']);
         fprintf([ ' left flank before reverse is ', leftFlank, '\n']);
         fprintf([ ' right flank before reverse is ', rightFlank, '\n']);
         %fprintf(['length is          ', len, '\n'])
         disp(['%%%%%%%%%%%%%%%%%%%']);
         % use the original values for context in cases where ref = A or C
         if (~isempty(findstr('ACGT', leftFlank)) && ~isempty(findstr('ACGT',  rightFlank)) && ~isempty(findstr('ACGT', altstring)) && ~isempty(findstr('ACGT',refstring)))   
           disp(['%%%%%%%%%%%%%%%%%%%']);       
           mutString = strcat(leftFlank, '(', refstring, '->', altstring, ')', rightFlank);
        
       
          % in cases where ref is G or T the reverse complement is used
          % in case ref is G :
             if (refstring == 'G') 
             refstringnew = complement(refstring);
             altstringnew = complement(altstring);
             rightFlanknew = complement(leftFlank);
             leftFlanknew = complement(rightFlank);
            
                fprintf(['ref string is ', refstringnew, '\n']);
                fprintf (['alt string is ', altstringnew, '\n']);
                fprintf(['right flank is ', rightFlanknew, '\n']);
                fprintf(['left flank is ', leftFlanknew, '\n']);
          mutString = strcat(leftFlanknew, '(', refstringnew, '->', altstringnew, ')', rightFlanknew);
            end
        % in case ref is T :
            if (refstring == 'T')
             refstringnew = complement(refstring);
             altstringnew = complement(altstring);
             rightFlanknew = complement(leftFlank);
             leftFlanknew = complement(rightFlank);
                
                fprintf(['ref string is ', refstringnew, '\n']);
                fprintf (['alt string is ', altstringnew, '\n']);
                fprintf(['right flank is ', rightFlanknew, '\n']);
                fprintf(['left flank is ', leftFlanknew, '\n']);
         mutString = strcat(leftFlanknew, '(', refstringnew, '->', altstringnew, ')', rightFlanknew);
           end
        
      

       elseif ~(~isempty(findstr('ACGT', leftFlank)) && ~isempty(findstr('ACGT',  rightFlank)) && ~isempty(findstr('ACGT', altstring)) && ~isempty(findstr('ACGT',refstring)) &&  len==21)
     
            mutString='N(N>N)N'  
        
       end
     elseif ~(len==21)
            mutString='N(N>N)N'
  end        
            

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author : Arnavaz Danesh
%  This script is to do lego plots for 
% Samples that show certain pattens of mutations that are
% not real and are read as mutation for other reasons
% the equivalent script is from broad called lego_plotter.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% The script first reads the maf file. The maf file should have been
%%% fitlered in advance for the Allelec fraction of interest.

%%%%%%%%
% Main %
%%%%%%%%

function [mutstring] = mutstring(mymaf)

    maf = readtable(mymaf,'HeaderLines',skipline,'Delimiter','\t', 'ReadVariableNames',true,'FileType', 'text');

    % Filtering the maf file :
    % Remove lines that do not have the context entry
    if strcmp(mymaf, 'Combined.txt')
        maf = maf(1:4000,:); 
    end

    % get the low allele fraction of the maf files
    maf_1 = maf(maf.i_tumor_f < AF_threshold1, :); % MAf for low allele fraction
    maf_2 = maf_1(maf_1.i_tumor_f > AF_threshold2, :);

    % free memory by removing the unwanted tables
    clear maf 
    % if interested in SNPs:
    if strcmp(variant_type,'SNP')
            % get rows only with SNP in the variant_type column
            snprows = find(~cellfun('isempty',strfind(maf_2.Variant_Type, 'SNP')));
            % get the part of maf with SNP as variant_type
            mafnew = maf_2(snprows,:);

    end

    % free memory by clearing these files
    clear maf_1;
    clear maf_2;

    % if interested in DNPs:
    if strcmp(variant_type, 'DNP')

        dnprows =  find(~cellfun('isempty',strfind(maf.Variant_Type, 'DNP')));
        mafnew = maf(dnprows,:);
    end

    % get the three necessary columns : Context , Alt and Ref 
    contexttable  =  mafnew(:, conAltRefCol);
    clear mafnew;
    % make the mutation context patterns table
    h = height(contexttable);
    mutString = cell(h,1);
    for i = 1: height(contexttable)

           mutStringNew = context(contexttable(i,:));
           mutString(i,1) = {mutStringNew};
           disp([i]);

    end

    %%  ind=find(ismember(b,'C(C->A)G' )) Find the index of an element 
    %%  Alternative: use of arrayfun/rowfun
    %% Make the lego matrix using the mutMatrix function



























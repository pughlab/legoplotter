function bar3_colors(varargin)
figure;
h = bar3(varargin{1:end-1});
ncols = length(h);
nrows = size(get(h(1),'zdata'),1)/6;

mat = varargin{1:end-1};
m1 = sum(sum(mat(1:4,1:4)));
m2 = sum(sum(mat(1:4,5:8)));
m3 = sum(sum(mat(1:4,9:12)));

m4 = sum(sum(mat(5:8,1:4)));
m5 = sum(sum(mat(5:8, 5:8)));
m6 = sum(sum(mat(5:8, 9:12)));

mut = [m3 m2 m1 m6 m5 m4];

C = varargin{end};
if size(C,1)~=nrows || size(C,2)~=ncols || size(C,3)~=3, error('C is wrong dimensions'); end

allcolors = zeros(ncols*nrows,3);
idx=1;
for y=1:nrows, 
    for x=1:ncols, allcolors(idx,:)=C(y,x,:); 
        idx=idx+1;
    end;
end
[u ui uj] = unique(allcolors,'rows');

colormap(u);
for x=1:ncols
  cdata = zeros(6*nrows,4);
  for y=1:nrows
    cdata((y-1)*6+1:y*6,:) = uj((y-1)*ncols+x);
  end
  set(h(x),'cdata',cdata);
end
% set the background color to white
set(gcf,'color',[1 1 1]);
colors = [1 1 0;0 0.7 0.7;1 0 0;0.1 0.8 0.1;0 0.2 0.8;0.5 0.3 0.7;];
% position and size for the pie chart
axes('Position', [0.2 ,.85, 0.1, 0.1]);
savefile = 'mut.mat';
%save(savefile,'mut');
p=pie(mut,repmat({''},length(mut),1));
set(gca,'xtick',[],'ytick',[],'ztick',[],'visible','off');

 for i=1:6
    set(p(2*i-1),'facecolor',colors(i,:))
 end;
 
vec = ['C->T';'C->A';'C->G';'A->G';'A->C';'A->T'];
h1=legend(vec);
set(h1,'Position',[.01 .8 .1 .2], 'Box', 'off')

% make the context matrix 
axes('Position', [0.58,0.02, .5, .5]);
q = {'T__T','T__C', 'T__A', 'T__G'; 'C__T', 'C__C', 'C__A', 'C__G'; 'A__T', 'A__C', 'A__A', 'A__G'; 'G__T', 'G__C', 'G__A', 'G__G'};
h=bar3(ones(4,4),'w');
set(h,'EdgeAlpha',0.25);
set(gca,'zlim',[0 50]);
for i=1:4,for j=1:4
  text((j-0.25),(i)-0.15,1,q(i,j),'FontSize',10,'horizontalAlign','left','EraseMode','none','interpreter','none');
end,end
set(gca,'xtick',[],'ytick',[],'ztick',[],'visible','off');
%set(gcf,'position',get(0,'screensize'));
%set(gcf, 'PaperPosition', [0 0 8 10]);


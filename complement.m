%%%% function for reverse complement of base pairs%%%%%%
function [complement] = complement(basepair)

         if (basepair == 'A')
             complement = 'T';
         elseif (basepair == 'C')
             complement = 'G';
         elseif (basepair == 'G')
             complement = 'C';
         elseif (basepair == 'T')
             complement = 'A';
         end
         
      
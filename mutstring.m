%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author : Arnavaz Danesh
%  This script is to do lego plots for 
% Samples that show certain pattens of mutations that are
% not real and are read as mutation for other reasons
% the equivalent script is from broad called lego_plotter.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% The script first reads the maf file. The maf file should have been
%%% fitlered in advance for the Allelec fraction of interest

function [mutString] = mutstring(contexttable)

   
   
    % make the mutation context patterns table
    h = height(contexttable);
    mutString = cell(h,1);
    for i = 1: height(contexttable)

           mutStringNew = context(contexttable(i,:));
           mutString(i,1) = {mutStringNew};
           disp([i]);

    end

    n = size(mutString)
    %%  ind=find(ismember(b,'C(C->A)G' )) Find the index of an element 
    %%  Alternative: use of arrayfun/rowfun
    %% Make the lego matrix using the mutMatrix function
  for i=1:n
   disp([n])
   disp([i])
   %if strcmp(mutString(i),'N(N>N)N')
     % mutString(i)=[]
   %end
 end 



   






















